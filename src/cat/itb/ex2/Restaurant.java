package cat.itb.ex2;

import java.util.concurrent.*;

public class Restaurant {

    public static void main(String[] args) {
        BlockingQueue<String> q = new ArrayBlockingQueue<>(5);

        ExecutorService executor = Executors.newCachedThreadPool();

        executor.submit(new Cuiner(q));
        executor.submit(new Cambrer(q));
        executor.submit(new Cambrer(q));
    }
}

class Cuiner implements Runnable {
    private BlockingQueue<String> cua;
    private ThreadLocalRandom random= ThreadLocalRandom.current();
    private String[] plats = {"Pasta al pesto", "Gachas", "Carpaccio de buey", "Tortilla de patatas", "Risotto de setas", "Paella"};

    public Cuiner(BlockingQueue<String> cua) {
        this.cua = cua;
    }

    @Override
    public void run() {
        try {
            while (true){
                cua.put(plats[random.nextInt(5)]);
                Thread.sleep(2000);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

class Cambrer implements Runnable {
    private BlockingQueue<String> cua;

    public Cambrer(BlockingQueue<String> cua) {
        this.cua = cua;
    }

    @Override
    public void run() {
        try {
            while (true){
                System.out.println(cua.take());
                Thread.sleep(1000);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
