package cat.itb.ex1;

import java.util.concurrent.ThreadLocalRandom;

public class MainXat {
    public static void main(String[] args) {
        Xat c=new Xat();
        Runnable u1=new UsuariPregunta(c);
        Runnable u2=new UsuariResposta(c);
        Thread t1=new Thread(u1);
        Thread t2=new Thread(u2);
        t1.start();
        t2.start();
    }
}

class Xat {
    public String question;
    public String answer;
    public boolean asked = false;

    public synchronized void pregunta (String msg){
        while (asked){
            try{
                wait();
            } catch (InterruptedException e) {}
        }
        asked = true;
        question = msg;
        notify();
    }

    public synchronized void resposta (String msg){
        while (!asked) {
            try {
                wait();
            } catch (InterruptedException e) {}
        }
        asked = false;
        answer = msg;
        notify();
    }
}

class UsuariPregunta implements Runnable{
    private Xat xat;
    private String[] preguntes = {"Saps qui és Messi?", "Has fet els deures?", "Tens germans?", "Ets extrovertit?", "Portes bé el curs?"};
    private ThreadLocalRandom tlr = ThreadLocalRandom.current();

    public UsuariPregunta(Xat xat) {
        this.xat = xat;
    }

    @Override
    public void run() {
        while (xat.question == null) {
            xat.pregunta(preguntes[(int) (Math.random()*5)]);
            System.out.println(xat.question);
        }
    }
}

class UsuariResposta implements Runnable{
    private Xat xat;
    private String[] respostes = {"Si", "No"};

    public UsuariResposta(Xat xat) {
        this.xat = xat;
    }

    @Override
    public void run() {
        while (xat.answer == null) {
            xat.resposta(respostes[(int) (Math.random()*2)]);
            System.out.println(xat.answer);
        }
    }
}


